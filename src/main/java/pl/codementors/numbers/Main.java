package pl.codementors.numbers;

import java.util.Scanner;
import java.lang.*;

public class Main {

    public static void main(String args[]) {

        Scanner scanner = new Scanner(System.in);

        int i;
        do {
            System.out.print("Enter an integer between 0 and 15: " + "\n");
            i = scanner.nextInt();

            if (i < 0 || i >= 15) {
                System.out.println("Invalid number");
            }
        } while (i < 0 || i >= 15);

        System.out.println("Number = " + i);
        System.out.println("Binary is " + Integer.toBinaryString(i));
        System.out.println("Number of one bits = " + Integer.bitCount(i));
    }

}